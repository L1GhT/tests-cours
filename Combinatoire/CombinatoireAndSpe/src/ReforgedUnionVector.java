import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

public class ReforgedUnionVector {
    /**
     * Méthode qui renvoi un vecteur aléatoire sur les deux passés en paramètres.
     * En indiquant le type d'un vecteur, la marge d'erreur est largement réduite.
     * De plus, le traitement des valeurs null est lui aussi appliqué autravers d'une autre méthode static.
     * @param a vecteur a
     * @param b vecteur b
     * @return Vector retourne un vecteur
     */
    public static Vector unionSet(Vector<String> a, Vector<String> b) {
        List<Vector> vectorList = new ArrayList<>();
        vectorList.add(removeNullValue(a));
        vectorList.add(removeNullValue(b));
        Random random = new Random();
        int i = random.nextInt(2);
        return vectorList.get(i);
    }

    public static Vector removeNullValue(Vector<String> vector) {
        Vector vectorToRemove = new Vector();
        for(String value: vector ) {
            if(value.equals(null))
                vectorToRemove.add(value);
        }
        vector.removeAll(vectorToRemove);
        return vector;
    }


}
