import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Vector;

import static org.junit.Assert.assertThat;

public class VectorUnionTest {

    VectorUnion vectorUnion = new VectorUnion();

    @Test
    /**
     * Nous pouvons déjà faire la remarque que si nous avons une différence de taille de deux collections, cela n'est pas vraiment pratique
     * pour tester la collection qui ressort de la méthode static.
     */
    public void should_test_vector_lenght() {
        Vector a = new Vector();;
        Vector b = new Vector();;
        a.add("test");
        a.add(null);
        a.add(123);

        b.add(null);
        b.add(12L);
        b.add("Yoiiiiiiiiiiiiiiiiiink");

        Vector c = vectorUnion.unionSet(a, b);
        Assertions.assertEquals(c.size(), a.size());
    }

    @Test
    /**
     * Tout va bien dans le meilleur des monde, même si des nulls sont présent. Tant pis pour la personne qui utilisera cette méthode, moi j'ai fais selon la spec !
     */
    public void should_test_vector_value() {
        Vector a = new Vector();
        ;
        Vector b = new Vector();
        ;
        a.add("test");
        a.add(null);
        a.add(123);

        b.add(null);
        b.add(12L);
        b.add("Yoiiiiiiiiiiiiiiiiiink");

        Vector c = vectorUnion.unionSet(a, b);
        Vector d = new Vector();

        if (c.get(0) == a.get(0))
            d = a;
        else
            d = b;

        for (int i = 0; i < d.size(); i++) {
            Assertions.assertEquals(c.get(i), d.get(i));
        }
    }

}
