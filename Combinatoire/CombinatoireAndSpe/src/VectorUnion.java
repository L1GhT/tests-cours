import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

class VectorUnion {


    public static void main(String[] args) {
        Vector<String> a = new Vector();
        Vector<String> b = new Vector();
        a.add("12");
        a.add("test");
        a.add(null);
        b.add("12L");
        b.add("0.1250000000000000000000");
        b.add(null);

        Vector c = unionSetReforged(a, b);
        System.out.println("test");
    }

    /**
     * Méthode qui renvoi un vecteur aléatoire sur les deux passés en paramètres.
     * @param a vecteur a
     * @param b vecteur b
     * @return Vector retourne un vecteur
     */
    public static Vector unionSet(Vector a, Vector b) {
        List<Vector> vectorList = new ArrayList<>();
        vectorList.add(a);
        vectorList.add(b);
        Random random = new Random();
        int i = random.nextInt(2);
        return vectorList.get(i);
    }

    /**
     * Méthode qui renvoi un vecteur aléatoire sur les deux passés en paramètres.
     * En indiquant le type d'un vecteur, la marge d'erreur est largement réduite.
     * De plus, le traitement des valeurs null est lui aussi appliqué autravers d'une autre méthode static.
     * @param a vecteur a
     * @param b vecteur b
     * @return Vector retourne un vecteur
     */
    public static Vector unionSetReforged(Vector<String> a, Vector<String> b) {
        List<Vector> vectorList = new ArrayList<>();
        vectorList.add(removeNullValue(a));
        vectorList.add(removeNullValue(b));
        Random random = new Random();
        int i = random.nextInt(2);
        return vectorList.get(i);
    }

    public static Vector removeNullValue(Vector<String> vector) {
        Vector vectorToRemove = new Vector();
        for(String value: vector ) {
            if(value == null)
                vectorToRemove.add(value);
        }
        vector.removeAll(vectorToRemove);
        return vector;
    }
}