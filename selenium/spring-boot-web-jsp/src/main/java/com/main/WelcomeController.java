package com.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.main.model.Money;
import com.main.model.MoneyBag;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class WelcomeController {

	private String message = "Hello World";

	@RequestMapping("/")
	public String welcome(Map<String, Object> model) {
		model.put("message", this.message);
		/*model.put("money1", new Money());
		model.put("money2", new Money());*/
		List<Money> moneys = new ArrayList<>();
		moneys.add(new Money());
		moneys.add(new Money());
		model.put("monies", moneys);
		return "welcome";
	}


	/*@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String registration(@ModelAttribute("money") Money money, @RequestAttribute("test2") String test, Map<String, Object> model) {
		System.out.println("test");
		MoneyBag moneyBag = new MoneyBag(new ArrayList<Money>());
		model.put("message", this.message);
		model.put("money", new Money());
		return "welcome";
	}*/

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String controllerMethod(HttpServletRequest request, Map<String, Object> model){
		// this way you get value of the input you want
		Money money1 = new Money(Integer.parseInt(request.getParameter("fAmount1")), request.getParameter("currency1"));
		Money money2 = new Money(Integer.parseInt(request.getParameter("fAmount2")), request.getParameter("currency2"));
		List<Money> moneyList = new ArrayList<>();
		moneyList.add(money1);
		moneyList.add(money2);
		MoneyBag moneyBag = new MoneyBag(moneyList);
		int result = moneyBag.getUniqDevise("UNIQ").getfAmount();
		model.put("result", result);
		return "welcome";
	}



}