package com.main.model;

import java.util.List;

public class MoneyBag {
    private List<Money> moneyList;

    public MoneyBag(final List<Money> monies) {
        this.moneyList = monies;
    }

    public void add(final Money m) {
        this.moneyList.add(m);
    }

    public void subb(final Money m) {
        this.moneyList.remove(m);
    }

    public Money getUniqDevise(final String currency) {
        Money m = new Money(0, currency);
        for(Money money: this.moneyList) {
            m.add(money.getfAmount(), m.getfCurrency());
        }
        return m;
    }

    public List<Money> getMoneyList() {
        return this.moneyList;
    }
}
