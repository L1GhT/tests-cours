<%@ page import="com.main.model.Money" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>

<link rel="stylesheet" type="text/css"
	href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />

<!-- 
	<spring:url value="/css/main.css" var="springCss" />
	<link href="${springCss}" rel="stylesheet" />
	 -->
<link href="${jstlCss}" rel="stylesheet" />

</head>
<body>

	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Spring Boot</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Home</a></li>
					<li><a href="#about">About</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container">

		<div class="starter-template">
			<h1>Spring Boot Web JSP Example</h1>
			<h2>Message: ${message}</h2>
		</div>

	</div>
	<!-- /.container -->

	<div class="container">
		<form method="POST" action="">
			<div class="row">
				<input name="fAmount1" type="number">
				<select name="fCurrency1">
					<option value="EUR">
						EUROS
					</option>
					<option value="USD">
						DOLLARS
					</option>
					<option value="test">
						TEST
					</option>
				</select>
			</div>
			<div class="row">
				<input name="fAmount2" type="number">
				<select name="fCurrency2">
					<option value="EUR">
						EUROS
					</option>
					<option value="USD">
						DOLLARS
					</option>
					<option value="test">
						TEST
					</option>
				</select>
			</div>

			<input type="submit" value="Submit" />
		</form>
	</div>
	<div class="container">
		<p>${result}</p>>
	</div>
	<script type="text/javascript"
		src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>

</html>
