import com.main.model.Money;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


public class MoneyTest {

    Money money;

    @Before
    public void setMoney() {
        this.money = new Money(10, "EUR");
    }

    @Test
    public void should_add_money_with_same_currency_with_params() {
        Assert.assertEquals(this.money.add(10, "EUR").getfAmount(), 20);
    }

    @Test
    public void should_return_null_value_when_currency_is_different_with_params() {
        Assert.assertNull(this.money.add(10, "CHF"));
        Assert.assertEquals(this.money.getfAmount(), 10);
    }

    @Test
    public void should_add_money_with_same_currency_with_object() {
        Money m = new Money(-10, "EUR");
        Assert.assertEquals(this.money.add(m).getfAmount(), 0);
    }

    @Test
    public void should_return_null_value_when_currency_is_different_with_object() {
        Money m = new Money(10,"CHF");
        Assert.assertNull(this.money.add(m));
        Assert.assertEquals(this.money.getfAmount(), 10);
    }
    @Test
    public void object_should_be_build() {
        assertThat(new Money(10, "EUR")).isEqualToComparingOnlyGivenFields(this.money, "fCurrency", "fAmount");
    }


}
