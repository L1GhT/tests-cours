import com.main.model.Money;
import com.main.model.MoneyBag;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThat;

public class MoneyBagTest {
    private MoneyBag moneyBag;
    private List<Money> moneyList;


    @Before
    public void init() {
        this.moneyList = new ArrayList<>();
        moneyList.add(new Money(10, "EUR"));
        moneyList.add(new Money(20, "USD"));
        this.moneyBag = new MoneyBag(moneyList);
    }

    @Test
    public void should_add_money_to_the_list() {
        Money m = new Money(30, "FRA");
        this.moneyBag.add(m);
        Assert.assertEquals(this.moneyBag.getMoneyList().size(), 3);
        assertThat(this.moneyBag.getMoneyList().get(2)).isEqualToComparingOnlyGivenFields(m, "fCurrency", "fAmount");
    }

    @Test
    public void should_sub_money_from_the_list() {
        this.moneyBag.subb(new Money(10, "EUR"));
        Assert.assertEquals(this.moneyBag.getMoneyList().size(), 2);
    }

    @Test
    public void should_return_list_of_money() {
        assertThat(this.moneyBag.getMoneyList().equals(this.moneyList)).isTrue();
    }

    @Test
    public void should_return_only_one_amount_from_list() {
        Money m = new Money(30, "yolo");
        assertThat(this.moneyBag.getUniqDevise("yolo")).isEqualToComparingOnlyGivenFields(m, "fCurrency", "fAmount");
    }
}
