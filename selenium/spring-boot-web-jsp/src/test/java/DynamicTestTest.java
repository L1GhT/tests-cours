import com.main.model.Money;
import com.main.model.MoneyBag;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

public class DynamicTestTest {

    private MoneyBag moneyBag = new MoneyBag(new ArrayList<Money>());;

    @TestFactory
    Stream<DynamicTest> testFactory() {

       List<Money> moneyList = new ArrayList<Money>();
       moneyList.add(new Money(50, "EUR"));
       moneyList.add(new Money(40, "USD"));
       moneyList.add(new Money(60, "test"));
       MoneyBag moneyBag = new MoneyBag(moneyList);

       return moneyBag.getMoneyList().stream().map(money -> dynamicTest("moneyyyy" + money.getfCurrency(), () -> {
           assertEquals(money, moneyList.get(moneyList.indexOf(money)));
       }));
    }

    @ParameterizedTest
    @MethodSource("getMoneyBag")
    public void should_test_ammount_sum(final Money money) {
        this.moneyBag.add(money);
        int sum = moneyBag.getMoneyList().stream().mapToInt(Money::getfAmount).sum() + 10;
        this.moneyBag.add(new Money(10, "USD"));
        assertEquals(moneyBag.getUniqDevise("TEST").getfAmount(), sum);
    }

    static Stream<Money> getMoneyBag() {
        Random random = new Random();
        return IntStream.range(10, 20).mapToObj(i -> new Money(random.nextInt(), "EUR"));
    }
}
