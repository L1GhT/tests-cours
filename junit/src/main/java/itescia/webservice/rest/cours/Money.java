package itescia.webservice.rest.cours;

public class Money {
    private int fAmount;
    private String fCurrency;

    public Money(final int amount, final String currency) {
        this.fAmount = amount;
        this.fCurrency = currency;
    }

    public int getfAmount() {
        return fAmount;
    }

    public String getfCurrency() {
        return fCurrency;
    }

    public Money add(Money m) {
        if(m.getfCurrency() == this.fCurrency) {
            m.add(this.fAmount, this.fCurrency);
            return m;
        }
        return null;
    }

    public Money add(final int amount, final String currency) {
        if(currency == this.fCurrency) {
            this.fAmount = this.fAmount + amount;
            return this;
        }
        return null;
    }
}
